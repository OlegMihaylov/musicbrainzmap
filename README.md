# MusicBrainz Map App

The user able to input search terms, and the App shows markers on the map related to the input term.
App implementet without 3rd party libraries.

1. AppContainer - container for manual DI in Application
2. SearchApi & ResponseParser - network implementation for requests to API. Use next API https://wiki.musicbrainz.org/Development
3. When app shows result, viewmodel checks openYear and, if it is empty, skip the item.
