package com.mikhailov.musicbrainzmap

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mikhailov.musicbrainzmap.di.AppContainer
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    private lateinit var mapView: MapView
    private lateinit var searchView: SearchView
    private var map: GoogleMap? = null

    private val viewModel: SearchViewModel by viewModels {
        SearchViewModel.provideFactory(getAppContainer())
    }
    private var items: List<UIPlace> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mapView = findViewById(R.id.mapView)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync {
            map = it
            processItems(items)
        }
        searchView = findViewById(R.id.searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.handleAction(SearchViewAction.Search(query))
                searchView.clearFocus()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
        subscribeToViewModelState()
        subscribeToViewModelEvents()
    }

    private fun subscribeToViewModelState() {
        lifecycleScope.launch {
            viewModel.state
                .flowWithLifecycle(lifecycle, Lifecycle.State.RESUMED)
                .collect {
                    items = it.itemList
                    processItems(items)
                }
        }
    }

    private fun subscribeToViewModelEvents() {
        lifecycleScope.launch {
            viewModel.events
                .flowWithLifecycle(lifecycle, Lifecycle.State.RESUMED)
                .collectLatest { event ->
                    when (event) {
                        SearchViewEvent.ShowError -> {
                            Toast.makeText(
                                this@MainActivity, getString(R.string.error_text),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        SearchViewEvent.ShowEmptyResult -> {
                            Toast.makeText(
                                this@MainActivity, getString(R.string.empty_result),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
        }
    }

    private fun processItems(itemList: List<UIPlace>) {
        val map = map ?: return
        map.clear()
        itemList.forEachIndexed { index, item ->
            if (index == 0) {
                map.moveCamera(CameraUpdateFactory.newLatLng(LatLng(item.latitude, item.longitude)))
            }
            map.addMarker(
                MarkerOptions()
                    .title(item.name)
                    .position(LatLng(item.latitude, item.longitude))
            )
        }
    }

    private fun getAppContainer(): AppContainer {
        return (application as MapApplication).appContainer
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

}