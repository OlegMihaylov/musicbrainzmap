package com.mikhailov.musicbrainzmap

import android.app.Application
import com.mikhailov.musicbrainzmap.di.AppContainer

class MapApplication : Application() {

    val appContainer = AppContainer()

}