package com.mikhailov.musicbrainzmap

sealed class SearchViewAction {
    data class Search(val query: String?): SearchViewAction()
}
