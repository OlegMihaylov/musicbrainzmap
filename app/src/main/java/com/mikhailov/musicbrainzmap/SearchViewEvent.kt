package com.mikhailov.musicbrainzmap

sealed class SearchViewEvent {
    object ShowError: SearchViewEvent()
    object ShowEmptyResult: SearchViewEvent()
}
