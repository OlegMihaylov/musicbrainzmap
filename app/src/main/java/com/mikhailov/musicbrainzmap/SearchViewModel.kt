package com.mikhailov.musicbrainzmap

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.mikhailov.musicbrainzmap.di.AppContainer
import com.mikhailov.musicbrainzmap.domain.Place
import java.util.Calendar
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class SearchViewModel(
    appContainer: AppContainer
): ViewModel() {

    private val searchApi = appContainer.searchApi

    private val _state = MutableStateFlow(MainScreenState(emptyList()))
    val state: StateFlow<MainScreenState> = _state.asStateFlow()

    private val _events = MutableSharedFlow<SearchViewEvent>()
    val events = _events.asSharedFlow()

    private var searchJob: Job? = null
    private var timerJob: Job? = null

    fun handleAction(action: SearchViewAction) {
        when(action) {
            is SearchViewAction.Search -> {
                val query = action.query ?: return
                search(query)
            }
        }
    }

    private fun search(query: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            try {
                val result = searchApi.search(query)
                val newItems = result.mapNotNull { it.toUIPlace() }
                _state.update { MainScreenState(newItems) }
                startLifetimeTimer()
                if (newItems.isEmpty()) {
                    _events.emit(SearchViewEvent.ShowEmptyResult)
                }
            } catch (e: Exception) {
                viewModelScope.launch {
                    _events.emit(SearchViewEvent.ShowError)
                }
            }
        }
    }

    private fun Place.toUIPlace(): UIPlace? {
        val openYear = this.openYear ?: return null
        if (openYear < THRESHOLD_YEAR) {
            return null
        }
        val currentTimeInMills = Calendar.getInstance().timeInMillis
        return UIPlace(
            id = this.id,
            name = this.name,
            latitude = this.latitude,
            longitude = this.longitude,
            showUntilMills = currentTimeInMills + (openYear - THRESHOLD_YEAR) * MILLS_IN_SECOND
        )
    }

    private fun startLifetimeTimer() {
        timerJob?.cancel()
        timerJob = viewModelScope.launch {
            while (true) {
                val currentTimeInMills = Calendar.getInstance().timeInMillis
                val newItemList = _state.value.itemList.filter { item ->
                    item.showUntilMills > currentTimeInMills
                }
                _state.update { MainScreenState(newItemList) }
                if (newItemList.isEmpty()) {
                    timerJob?.cancel()
                }
                delay(TIMER_DELAY)
            }
        }
    }

    companion object {
        const val THRESHOLD_YEAR = 1990
        const val TIMER_DELAY = 1000L
        const val MILLS_IN_SECOND = 1000

        fun provideFactory(appContainer: AppContainer): ViewModelProvider.Factory = viewModelFactory {
            initializer {
                SearchViewModel(appContainer)
            }
        }
    }

}