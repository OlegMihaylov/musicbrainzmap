package com.mikhailov.musicbrainzmap

data class UIPlace(
    val id: String,
    val name: String,
    val latitude: Double,
    val longitude: Double,
    val showUntilMills: Long
)