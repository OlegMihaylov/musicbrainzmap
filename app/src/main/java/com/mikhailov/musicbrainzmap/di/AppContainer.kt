package com.mikhailov.musicbrainzmap.di

import com.mikhailov.musicbrainzmap.network.ResponseParser
import com.mikhailov.musicbrainzmap.network.SearchApi

class AppContainer {

    val searchApi by lazy {
        SearchApi(responseParser)
    }

    private val responseParser by lazy {
        ResponseParser()
    }

}