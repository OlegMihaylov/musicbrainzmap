package com.mikhailov.musicbrainzmap.domain

data class Place(
    val id: String,
    val name: String,
    val latitude: Double,
    val longitude: Double,
    val openYear: Int?
)
