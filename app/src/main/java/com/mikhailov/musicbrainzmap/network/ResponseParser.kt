package com.mikhailov.musicbrainzmap.network

import com.mikhailov.musicbrainzmap.domain.Place
import org.json.JSONObject

class ResponseParser {

    fun parseResponse(jsonResponse: String): List<Place> {
        val result = mutableListOf<Place>()
        val jsonObject = JSONObject(jsonResponse)
        if (!jsonObject.has(PLACES_KEY)) {
            return result
        }
        val placesJson = jsonObject.getJSONArray(PLACES_KEY)
        for (i in 0 until placesJson.length()) {
            val item = placesJson.getJSONObject(i)
            val place = parsePlace(item)
            if (place != null) {
                result.add(place)
            }
        }
        return result
    }

    private fun parsePlace(jsonObject: JSONObject): Place? {
        val id = jsonObject.getString(ID_KEY)
        val name = jsonObject.getString(NAME_KEY)

        if (!jsonObject.has(COORDINATES_KEY)) return null
        val coordinates = jsonObject.getJSONObject(COORDINATES_KEY)

        val latitude = coordinates.getDouble(LATITUDE_KEY)
        val longitude = coordinates.getDouble(LONGITUDE_KEY)
        val openYear = processLifeSpanBlock(jsonObject)
        return Place(
            id = id,
            name = name,
            latitude = latitude,
            longitude = longitude,
            openYear = openYear
        )
    }

    private fun processLifeSpanBlock(jsonObject: JSONObject): Int? {
        if (!jsonObject.has(LIFE_SPAN_KEY)) {
            return null
        }
        val openYearBlock = jsonObject.getJSONObject(LIFE_SPAN_KEY)
        if (!openYearBlock.has(LIFE_SPAN_BEGIN_KEY)) {
            return null
        }
        return try {
            openYearBlock.getString(LIFE_SPAN_BEGIN_KEY).substring(0, 4).toInt()
        } catch (e: Exception) {
            null
        }
    }

    companion object {
        const val PLACES_KEY = "places"
        const val ID_KEY = "id"
        const val NAME_KEY = "name"
        const val COORDINATES_KEY = "coordinates"
        const val LATITUDE_KEY = "latitude"
        const val LONGITUDE_KEY = "longitude"
        const val LIFE_SPAN_KEY = "life-span"
        const val LIFE_SPAN_BEGIN_KEY = "begin"
    }

}