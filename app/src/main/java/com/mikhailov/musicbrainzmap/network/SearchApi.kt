package com.mikhailov.musicbrainzmap.network

import com.mikhailov.musicbrainzmap.domain.Place
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SearchApi(
    private val responseParser: ResponseParser
) {

    suspend fun search(query: String): List<Place> {
        return withContext(Dispatchers.IO) {
            val url = URL(API_URL + query)
            val connection = url.openConnection() as HttpURLConnection
            val code = connection.responseCode
            if (code != STATUS_OK) {
                throw IOException("Network error with code: $code")
            }

            val bufferReader = BufferedReader(
                InputStreamReader(connection.inputStream)
            )
            val jsonStringBuilder = StringBuilder()
            var line = bufferReader.readLine()
            while(line != null) {
                jsonStringBuilder.append(line)
                line = bufferReader.readLine()
            }
            bufferReader.close()
            responseParser.parseResponse(jsonStringBuilder.toString())
        }
    }

    companion object {
        const val STATUS_OK = 200
        const val API_URL = "https://musicbrainz.org/ws/2/place/?fmt=json&query="
    }

}