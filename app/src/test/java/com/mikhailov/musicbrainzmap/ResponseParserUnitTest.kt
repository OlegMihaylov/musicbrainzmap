package com.mikhailov.musicbrainzmap

import com.mikhailov.musicbrainzmap.domain.Place
import com.mikhailov.musicbrainzmap.network.ResponseParser
import org.junit.Assert.assertEquals
import org.junit.Test

class ResponseParserUnitTest {

    @Test
    fun testParser() {
        val responseParser = ResponseParser()

        val result = responseParser.parseResponse("{}")
        assertEquals(result, emptyList<Place>())
    }

    @Test
    fun testEmptyResult() {
        val responseParser = ResponseParser()
        val result = responseParser.parseResponse(TEST_JSON)
        assertEquals(result, listOf(expectedPlace))
    }

    companion object {
        const val TEST_JSON = "{\n" +
                "  \"created\": \"2023-09-28T21:59:53.970Z\",\n" +
                "  \"count\": 61,\n" +
                "  \"offset\": 0,\n" +
                "  \"places\": [\n" +
                "    {\n" +
                "      \"id\": \"a8b04ff6-2aa6-46e6-a5c2-62ec298f4935\",\n" +
                "      \"type\": \"Venue\",\n" +
                "      \"type-id\": \"cd92781a-a73f-30e8-a430-55d7521338db\",\n" +
                "      \"score\": 100,\n" +
                "      \"name\": \"Coliseu do Porto\",\n" +
                "      \"coordinates\": {\n" +
                "        \"latitude\": \"41.146778\",\n" +
                "        \"longitude\": \"-8.605511\"\n" +
                "      },\n" +
                "      \"area\": {\n" +
                "        \"id\": \"309f8f03-51b5-4640-b4cf-d1a5e022ccf4\",\n" +
                "        \"type\": \"City\",\n" +
                "        \"type-id\": \"6fd8f29a-3d0a-32fc-980d-ea697b69da78\",\n" +
                "        \"name\": \"Porto\",\n" +
                "        \"sort-name\": \"Porto\",\n" +
                "        \"life-span\": {\n" +
                "          \"ended\": null\n" +
                "        }\n" +
                "      },\n" +
                "      \"life-span\": {\n" +
                "        \"begin\": \"1941-12-19\",\n" +
                "        \"ended\": null\n" +
                "      },\n" +
                "      \"aliases\": [\n" +
                "        {\n" +
                "          \"sort-name\": \"Coliseum of Porto\",\n" +
                "          \"type-id\": \"fb68f9a2-622c-319b-83b0-bbff4127cdc5\",\n" +
                "          \"name\": \"Coliseum of Porto\",\n" +
                "          \"locale\": \"en\",\n" +
                "          \"type\": \"Place name\",\n" +
                "          \"primary\": null,\n" +
                "          \"begin-date\": null,\n" +
                "          \"end-date\": null\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}"

        private val expectedPlace = Place(
            id = "a8b04ff6-2aa6-46e6-a5c2-62ec298f4935",
            name = "Coliseu do Porto",
            latitude = 41.146778,
            longitude = -8.605511,
            openYear = 1941
        )
    }
}